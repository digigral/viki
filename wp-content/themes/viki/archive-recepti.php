<?php
/**
 * The template for displaying archive pages
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="archive-wrapper">
    <section class="extFeatures cid-rR4yAnKzKX" id="extFeatures36-1y">
        <div class="container">
            <div class="row justify-content-center">
            <?php if ( have_posts() ) : ?>
                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="<?php echo get_post_permalink($post ->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post ->ID)?>" alt="" title=""></a>
                        </div>
                        <div class="card-box align-center">
                            <h3 class="card-title mbr-fonts-style display-5"><?php echo $post->post_title ?><br></h3>
                        </div>
                    </div>
                </div>
            <?php endwhile;
            endif; ?>
            </div>
        </div>
    </section>
</div><!-- #archive-wrapper -->

<?php get_footer();
