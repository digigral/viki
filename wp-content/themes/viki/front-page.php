<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="index-wrapper">

	<section class="extFeatures cid-rR4vejasKe" id="extFeatures29-1v">

       <div class="container">
            <h2 class="mbr-fonts-style mb-4 align-center display-2"><strong><?php echo  get_field('naslov_strani');?></strong></h2>
            <h3 class="mbr-section-subtitle mbr-fonts-style mb-5 mbr-light align-center display-7"><?php echo  get_field('podnaslov_strani');?></h3>
            <div class="media-container-row">
                <?php $izdelki = get_field('izdelki');
                if($izdelki):
                    foreach ($izdelki as $item):?>&nbsp;
                        <div class="card col-12 col-md-4 col-lg-4">
                            <div class="card-img">
                                <img src="<?php echo $item['slika_izdelka']?>" alt="" title="">
                            </div>
                            <div class="card-box">
                                <p class="date mb-4">
                                    <span><?php echo $item['oznaka']?></span></p>
                                <h4 class="card-title mbr-fonts-style display-5">
                                    <?php echo $item['ime_izdelka']?></h4>
                                <p class="mbr-text mbr-fonts-style display-7">
                                    <?php echo $item['opis_izdelka']?>
                                </p>
                                <div class="mbr-section-btn"><a class="btn-underline mr-3 display-7" href="<?php echo $item['link_izdelka']?>">VEČ &gt;</a></div>
                            </div>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
	</section>

	<div>
		<?php get_template_part('/page-templates/templates-part/fb-feed') ?>
	</div>

	<section class="extFeatures cid-rR4yAnKzKX" id="extFeatures36-1y">
		<div class="container">

			<div class="row justify-content-center pb-4">
				<div class="col-md-6 align-left">
					<h2 class="mbr-section-title mbr-bold pb-1 mbr-fonts-style display-1"><?php echo get_field('naslov_receptov'); ?></h2>
				</div>
				<div class="col-md-6 align-right btn-col">
					<div class="mbr-section-btn"><a class="btn btn-sm btn-warning-outline display-4" href="#">VEČ IDEJ</a></div>
				</div>
			</div>

			<div class="row justify-content-center">
				<?php
				$args = array(
					'post_type' => 'recepti',
					'posts_per_page' => 5
				);
				$recepti = new WP_Query( $args );
				if($recepti->posts):
					foreach ($recepti->posts as $post):
						?>
						<div class="card p-3 col-12 col-md-6 col-lg-4">
							<div class="card-wrapper">
								<div class="card-img">
									<a href="<?php echo get_post_permalink($post ->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post ->ID)?>" alt="" title=""></a>
								</div>
								<div class="card-box align-center">
									<h3 class="card-title mbr-fonts-style display-5"><?php echo $post->post_title ?><br></h3>
								</div>
							</div>
						</div>
					<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</section>

	<section class="extFeatures cid-rR4yvWGYIo" id="extFeatures22-1w">

		<div class="container">
			<h4 class="main-title pb-5 align-left mbr-regular mbr-fonts-style display-2"><?php echo  get_field('novice_naslov');?></h4>

			<div class="row justify-content-center">
            <?php
            $args = array(
                'post_type' => 'novice',
                'posts_per_page' => 5
            );
            $novice = new WP_Query( $args );
            if($novice->posts):
                foreach ($novice->posts as $n):
                    ?>
				<div class="card p-3 col-12 col-md-6 col-lg-4">
					<div class="card-wrapper">
						<div class="card-img">
							<a href="<?php echo get_post_permalink($n ->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($n ->ID)?>" alt="" title=""></a>
						</div>
						<div class="card-box align-left">
							<h4 class="card-title align-left pb-3 mbr-fonts-style display-5"><?php echo $n->post_title ?></h4>
							<p class="mbr-text align-left mbr-fonts-style display-4"><?php echo $n->post_excerpt; ?></p>

							<div class="mbr-link-btn"><a class="btn btn-md btn-warning-outline display-4" href="<?php echo get_post_permalink($n ->ID); ?>"><span class="mobi-mbri mobi-mbri-right mbr-iconfont mbr-iconfont-btn"></span>PREBERI VEČ</a></div>

						</div>
					</div>
				</div>

                <?php   endforeach;
					endif;
					?>
			</div>
		</div>
	</section>

	<section class="cid-rR4CdrZpHI" id="title02-24">
		<div class="container align-center">
			<div class="row justify-content-md-start justify-content-md-center">
				<div class="mbr-white col-sm-12 col-md-10 col-lg-7">
					<h1 class="mbr-section-title mbr-regular align-center mbr-fonts-style display-2"><?php echo  get_field('video_naslov');?></h1>
				</div>
			</div>
		</div>
	</section>

	<section class="header8 cid-rR4BUqlCQt" id="video02-22">
		<div class="container align-center">
			<div class="row justify-content-md-center">
				<div class="mbr-white box col-md-12 col-12 col-lg-10">
					<div class="mbr-media show-modal align-center py-2" data-modal=".modalWindow">
						<div class="icon-wrap">
							<span class="mbr-iconfont mobi-mbri-play mobi-mbri"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>
			<div class="modalWindow" style="display: none;">
				<div class="modalWindow-container">
					<div class="modalWindow-video-container">
						<div class="modalWindow-video">
							<iframe width="100%" height="100%" frameborder="0" allowfullscreen="1" data-src="https://www.youtube.com/watch?v=J21qu_vspfg"></iframe>
						</div>
						<a class="close" role="button" data-dismiss="modal">
							<span aria-hidden="true" class="mbri-close mbr-iconfont closeModal"></span>
							<span class="sr-only">Close</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>



</div>
<?php get_footer();
