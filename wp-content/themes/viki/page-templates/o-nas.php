<?php
/**
 * Template Name: O nas
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

<section class="header4 cid-rR4IyQnCoH" id="content05-2r">





    <div class="container">
        <div class="row justify-content-md-center">
            <div class=" col-md-12 col-lg-10 align-left">

                <h1 class="mbr-section-title align-left mbr-white pb-2 mbr-fonts-style display-2"><?php echo get_the_title();?> </h1>

                <p class="mbr-text align-left mbr-fonts-style display-7">
                   <?php echo $post->post_content ?> <br></p>


            </div>



        </div>
    </div>
</section>

<section class="extMap cid-rR4IKeTUFg mbr-parallax-background" id="extMap3-2u">



    <div class="mbr-overlay" style="opacity: 0.4; background-color: rgb(255, 255, 255);">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-4 col-md-6 text-block">
                <div class="content-panel">


                    <p class="adress-block mbr-fonts-style align-left mbr-white display-7">Naziv:<br>Petlja trgovina in storitve d.o.o.<br>Ob Dravi 3a, 2251 Ptuj<br>Slovenija<br><br>Telefon: 02 788 00 30<br>E-pošta: melita@petlja.si<br><br>Direktor:<br>Branko Rojs<br><br>ID za DDV : SI73943894<br>Matična številka: 5324467<br><br>BANKA:<br>SKB Banka d.d.<br>Ajdovščina 4<br>1513 Ljubljana, SLOVENIJA<br>IBAN Številka: SI56031821000010113<br>SWIFT - SKBASI2X</p>
                </div>
            </div>
            <div class="col-sm-12 col-lg-8 col-md-6">
                <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAEIpgj38KyLFELm2bK9Y7krBkz1K-cMq8&amp;q=place_id:ChIJaZhWWUphb0cRK8OQeap71c0" allowfullscreen=""></iframe></div>
            </div>
        </div>
    </div>
</section>

<section class="extFeatures cid-rR4yvWGYIo" id="extFeatures22-1w">

        <div class="container">
            <h4 class="main-title pb-5 align-left mbr-regular mbr-fonts-style display-2">Novo pri nas</h4>

            <div class="row justify-content-center">
                <?php
                $args = array(
                    'post_type' => 'novice',
                    'posts_per_page' => 5
                );
                $novice = new WP_Query( $args );
                if($novice->posts):
                    foreach ($novice->posts as $post):
                        ?>
                        <div class="card p-3 col-12 col-md-6 col-lg-4">
                            <div class="card-wrapper">
                                <div class="card-img">
                                    <a href="<?php echo get_post_permalink($post ->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post ->ID)?>" alt="" title=""></a>
                                </div>
                                <div class="card-box align-left">
                                    <h4 class="card-title align-left pb-3 mbr-fonts-style display-5"><?php echo $post->post_title ?></h4>
                                    <p class="mbr-text align-left mbr-fonts-style display-4"><?php echo $post->post_excerpt; ?></p>

                                    <div class="mbr-link-btn"><a class="btn btn-md btn-warning-outline display-4" href="<?php echo get_post_permalink($post ->ID); ?>"><span class="mobi-mbri mobi-mbri-right mbr-iconfont mbr-iconfont-btn"></span>PREBERI VEČ</a></div>

                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </section>

<?php get_footer();