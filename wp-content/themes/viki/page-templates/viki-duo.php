<?php
/**
 * Template Name: Viki Duo
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
?>

    <section class="header1 cid-rR40naMVKK" id="header01-1e">






        <svg class="svg1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px" height="810px" viewBox="0 0  1380 810" preserveAspectRatio="xMidYMid meet">
            <rect id="svgEditorBackground" x="0" y="0" width="1380" height="810" style="fill: none; stroke: none;"></rect>
            <circle id="e1_circle" cx="309" cy="433" style="fill:aqua;stroke:black;stroke-width:0px;" r="149.486"></circle>
            <circle id="e3_circle" cx="679" cy="707" style="fill: aqua; stroke-width: 0px;stroke:black;" r="516.715">
            </circle>
            <circle id="e4_circle" cx="759" cy="261" style="fill:aqua;stroke:black;stroke-width:0px;" r="132.2459829258">
            </circle>
            <circle id="e5_circle" cx="1054" cy="462" style="fill:aqua;stroke:black;stroke-width:0px;" r="139.413"></circle>
            <circle id="e6_circle" cx="691" cy="692" style="fill:khaki;stroke:black;stroke-width:0px;" r="367.214"></circle>
        </svg>

        <div class="round round1 rev"></div>
        <div class="round round2 rev"></div>
        <div class="round round3"></div>
        <div class="round round4 rev"></div>
        <div class="round round5"></div>
        <div class="round round6 rev"></div>
        <div class="round round7"></div>
        <div class="round round8 rev"></div>
        <div class="round round9 rev"></div>
        <div class="round round10"></div>
        <div class="round round11"></div>

        <div class="container">
            <div class="row justify-content-md-center">


                <div class="col-md-10 align-center">
                    <h1 class="mbr-section-title mbr-regular pb-2 mbr-fonts-style display-1"><strong>Viki za vse generacije,<br>že več kot 30 let.</strong></h1>

                    <p class="mbr-text pb-2 mbr-regular mbr-fonts-style display-5"><strong>Mleko, kakav in lešniki.</strong><br></p>

                </div>

                <div class="pt-4 align-center">
                    <img src="/wp-content/themes/viki/assets/images/viki-duo-3pack-1-753x793.png" alt="" title="" style="width: 90%;">
                </div>

            </div>
        </div>

    </section>

    <section class="extHeader cid-rR4b6iSa6d" id="extHeader24-1m">





        <div class="container align-left">
            <div class="row ">
                <div class="col-lg-8 col-md-12 py-lg-0 pb-4">
                    <div class="img-wrap">
                        <img src="/wp-content/themes/viki/assets/images/viki-1-939x1080.jpg" alt="" title="">
                    </div>
                </div>
                <div class="align-self-center mbr-black col-lg-4 col-md-12 py-lg-0 pt-2">
                    <div class="card-wrap">

                        <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-7">VIKI Duo je proizvedena iz naravnih surovin – mleka, kakava ter lešnikov. VIKI Duo ima svoj čar v svojem okusu in dvobarvnosti, ki ostaja skozi desetletja vedno isti. V vsaki VIKI Duo je polovica temne čokoladne in polovica bele vanilijeve kreme.</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="extHeader cid-rR4eeSeJll" id="extHeader24-1o">





        <div class="container align-left">
            <div class="row ">
                <div class="col-lg-8 col-md-12 py-lg-0 pt-4 order-2">
                    <div class="img-wrap">
                        <img src="/wp-content/themes/viki/assets/images/viki2-696x800.jpg" alt="" title="">
                    </div>
                </div>
                <div class="align-self-center mbr-black col-lg-4 col-md-12 py-lg-0 pb-2">
                    <div class="card-wrap">

                        <h3 class="mbr-section-subtitle align-center mbr-light mbr-fonts-style display-7">Dvobarvna krema je sestavljena iz polovice čokoladnega in polovice vanilijevega namaza. Čokoladnemu namazu daje čokoladno barvo in čokoladni okus kakav, ki je istočasno pomemben vir antioksidantov. Vanilijev del ne vsebuje kakava, zato pa vsebuje veliko mleka v prahu. Mleko v prahu vsebuje veliko kalcija, ki je pomemben za izgradnjo kosti.</h3>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features9 cid-rR4fw6dQ5W" id="extFeatures47-1r">




        <div class="container-fluid">
            <div class="row">
                <div class="card col-12 col-md-6 col-lg-3">

                    <div class="card-img">
                        <img src="/wp-content/themes/viki/assets/images/viki4-930x900.jpg" alt="" title="">
                    </div>

                </div>

                <div class="card col-12 col-md-6 col-lg-3">
                    <div class="card-wrapper">

                        <h4 class="card-title align-left mbr-fonts-style display-7">Nekateri imajo rajši čokoladni del, drugi beli vanilijev del, če pa si oba dela skupaj namažemo na kruh se oba okusa skupaj se zlijeta v odličen okus, ki je že več desetletij sinonim za okusno in hranilno dvobarvno kremo.<div><br></div><div>VIKI duo lahko preprosto uživamo z žličko, odlično pa se maže na kruh. Z VIKI duo se lahko namažejo tudi palačinke, lahko se uporabi kot sestavina za sladoled, sladice, torte, in drugo pecivo.</div></h4>




                    </div>
                </div>




                <div class="card col-12 col-md-12 col-lg-6">

                    <div class="card-img">
                        <img src="/wp-content/themes/viki/assets/images/viki3-1279x678.jpg" alt="" title="">
                    </div>

                </div>


            </div>
        </div>
    </section>

    <section class="extFeatures cid-rR4ngPmVgI" id="extFeatures28-1t">




        <div class="container">
            <div class="media-container-row">
                <div class="card col-12 col-md-4">
                    <h2 class="mbr-fonts-style mb-4 mbr-section-title display-5">Inovativni Viki namazi</h2>
                    <h3 class="mbr-text section-text mbr-fonts-style mbr-light display-7">V 2019 smo kupcem predstavili dva izredno zanimiva nova izdelka. Viki Premium vsebuje za 30 % manj sladkorja, Viki Protein pa se ponaša z višjo vsebnostjo beljakovin. Preverite oba in si izberite najljubšega.&nbsp;</h3>

                </div>
                <div class="card col-12 col-md-4">
                    <div class="card-img">
                        <img src="/wp-content/themes/viki/assets/images/viki-premium03-novo-753x793.png" alt="" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-5">
                            Viki Premium 30 % manj sladkorja</h4>

                        <div class="mbr-section-btn"><a class="btn-underline mr-3 text-danger display-5" href="#">Preveri</a></div>
                    </div>
                </div>
                <div class="card col-12 col-md-4">
                    <div class="card-img">
                        <img src="/wp-content/themes/viki/assets/images/viki-protein-753x793.png" alt="" title="">
                    </div>
                    <div class="card-box">
                        <h4 class="card-title mbr-fonts-style display-5">
                            Viki Protein namaz</h4>

                        <div class="mbr-section-btn"><a class="btn-underline mr-3 text-info display-5" href="#">Preveri</a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer();