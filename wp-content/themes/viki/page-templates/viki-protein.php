<?php
/**
 * Template Name: Viki Protein
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
get_header();
wp_enqueue_style( 'protein-css' );
wp_enqueue_style( 'gallery-css' );
wp_enqueue_script( 'gallery' );

?>


    <section class="header1 cid-rIsFO1vV0z mbr-parallax-background" id="header05-10">
        <div class="mbr-overlay" style="opacity: 0.6; background-color: rgb(113, 108, 128);">
        </div>


        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px" height="760px" viewBox="0 0 1380 760" preserveAspectRatio="xMidYMid meet">
            <path d="M0.3577131120350206,0.819491525482845h-1.5000000000000355ZM0.3577131120350206,-3.1805084745172603h-1.5000000000000355ZM-0.14228688796500222,-4.180508474517258h5.000000000000002a5,5,0,0,1,0,6.00000000000003h-5.000000000000025a5,5,0,0,0,0,-6.00000000000003ZM5.8577131120349835,-1.1805084745172634h1.0000000000000249Z" style="fill:white; stroke:black; vector-effect:non-scaling-stroke;stroke-width:0px;" id="e2_shape" transform="matrix(1.01506 82.3743 -245.478 0.34062 392.311 526.125)"></path>
        </svg>


        <div class="container">

            <div class="row justify-content-md-center">

                <div class="pt-5 align-center" style="margin-bottom: 30px; margin-top: -50px !important;">
                    <img src="/wp-content/themes/viki/assets/images/logo.png" alt="" title="" style="width: 95%;">
                </div>

                <div class="col-md-10 col-lg-7 align-center">

                    <h3 class="mbr-section-subtitle mbr-regular pb-2 mbr-fonts-style display-2"><strong>Nova, drugačna, a še vedno vsem poznana, s kakavom, lešniki in 16 % beljakovin.&nbsp;</strong></h3>
                    <p class="mbr-text pb-2 mbr-regular mbr-white mbr-fonts-style display-5"><strong>
                            Brez dodanega sladkorja &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Brez glutena </strong></p>

                </div>

                <div class="pt-5 align-center">
                    <img src="/wp-content/themes/viki/assets/images/viki-protein-753x793.png" alt="" title="" style="width: 80%;">
                </div>

            </div>
        </div>

    </section>

    <section class="cid-rIrYArtSKk" id="header06-v">

        <div class="container align-center">
            <div class="row">
                <div class="col-md-12 col-lg-6 py-5 m-auto">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-left mbr-fonts-style display-2"><strong>Novo v družini Viki</strong></h1>
                    <h3 class="mbr-section-subtitle align-left align-left mbr-regular pb-3 mbr-fonts-style display-5">Nova Viki krema Protein z lešniki, kakavom in brez dodanega sladkorja je primerna za čisto vse. Za športne po duhu, za sladkosnede, za tiste s slabo vestjo ali tiste brez nje.</h3>
                    <p class="mbr-text mbr-light pb-3 align-left mbr-fonts-style display-7">Za prijatelje, tete, strice, babice, očete in seveda za otroke. Za tiste, ki nikoli ne nehate hraniti otroka v sebi, ki bo z okusom lešnika oživel, se z okusom kakava potopil v čase vsem poznane Viki kreme, čase brez skrbi in brez slabe vesti. Naj bodo taki spomini tudi spomini naših otrok. Postrezite jo!</p>
                </div>

                <div class="col-lg-6 col-md-12 relative align-right">
                    <img class="img1" src="/wp-content/themes/viki/assets/images/viki_spletna_30.jpg" alt="" title="">
                    <img class="img2" src="/wp-content/themes/viki/assets/images/viki-protein-solo-684x720.png" alt="" title="">

                </div>
            </div>
        </div>

    </section>

    <section class="cid-rIrYArZFvm" id="header08-w">
        <div class="container align-center">
            <div class="row justify-content-md-start justify-content-md-center">
                <div class="mbr-white col-sm-212 col-md-10 col-lg-7">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-center mbr-fonts-style display-5"><strong>Viki Protein čaka na vas v bolje založenih trgovinah</strong><br><strong>po Sloveniji.</strong></h1>
                </div>
            </div>
        </div>

    </section>

    <section class="cid-rIt4OGIhGC" id="header03-12">

        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380" height="810px" viewBox="0 -8.992806499463768e-14 1380 810" preserveAspectRatio="xMidYMid meet">
            <rect id="svgEditorBackground" x="0" y="0" width="1380" height="810" style="fill: none; stroke: none;"></rect>
            <defs id="svgEditorDefs">
                <polygon id="svgEditorShapeDefs" style="fill:khaki;stroke:black;vector-effect:non-scaling-stroke;stroke-width:1px;"></polygon>
                <path id="svgEditorClosePathDefs" style="stroke:black;stroke-width:1px;fill:khaki;"></path>
            </defs>
            <path d="M15083.791600683318,645.6720847341486c496.6000000000058,747.7999999999951,1953.3704016668416,1862.6734725832403,2751.999999999978,1967.9999999999955s1884.300000000003,-198.89999999999964,2718.7056474818382,-552.6365690040448s1746.4898225846773,-519.9524548776521,2593.2943525181545,-567.3634309959566s2251,-23.399999999999636,4384,1520.0000000000032s3205.199999999997,1437.2000000000025,4047.9999999999854,1520.0000000000018s3305.000000000029,-270.2999999999993,4640.000000000029,-1152s74.60000000000582,3328.3000000000065,32,3792.0000000000073s-19509.4,4591.000000000004,-21087.999999999993,-128.00000000000182s-1679.5999999999913,-6358.300000000001,-80,-6400Z" style="stroke:black;fill:khaki;stroke-width:0px;" id="e12_areaS3" transform="matrix(0.0650006 -0.00123765 0.00123765 0.0650006 -995.543 317.589)"></path>
            <path d="M12434.469938292996,-979.2324603008726c496.600000000004,747.8000000000001,1953.3704016668398,1862.673472583247,2752.000000000018,1967.9999999999995s1884.2999999999865,-198.90000000000055,2718.7056474818364,-552.6365690040473s1746.4898225846846,-519.9524548776479,2593.2943525181618,-567.3634309959524s2251,-23.400000000001455,4384,1520s3205.199999999997,1437.1999999999962,4048,1519.9999999999955s3304.9999999999927,-270.3000000000011,4640,-1151.9999999999955s74.60000000000582,3328.3,32,3791.9999999999955s-19509.400000000023,4590.999999999997,-21088.000000000015,-127.99999999999818s-1679.5999999999985,-6358.300000000008,-80,-6400.0000000000055Z" style="stroke:black;fill:aqua;stroke-width:0px;" id="e19_areaS3" transform="matrix(0.0647268 0.00608646 -0.00608646 0.0647268 -823.261 331.266)"></path><text dy="-0.5em" style="fill:black;font-family:Arial;font-size:20px;" id="e14_texte" transform="matrix(0.0640454 0 0 0.0640454 -832.187 329.878)">
                <textPath id="e13_textPath" xlink:href="#e12_areaS3">T</textPath>
            </text>
        </svg>

        <div class="container align-center">
            <div class="row">
                <div class="col-md-12 col-lg-6 py-4 m-auto">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-left mbr-fonts-style display-2"><strong>Odkrijte tudi</strong><div><strong>Viki Premium s</strong></div><div><strong>30% manj sladkorja&nbsp;</strong></div></h1>


                    <div class="align-left mbr-section-btn">
                        <a class="btn btn-md btn-primary display-4" href="http://vikikrema.si/" target="_blank">IZVEJTE VEČ</a>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 align-center">
                    <img src="/wp-content/themes/viki/assets/images/viki-premium03-novo-753x793.png" alt="" title="">

                </div>
            </div>



        </div>

    </section>


    <section class="cid-rFQ7BERBLW" id="header08-h">
        <div class="container align-center">
            <div class="row justify-content-md-start justify-content-md-center">
                <div class="mbr-white col-sm-212 col-md-10 col-lg-7">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-center mbr-fonts-style display-2"><strong>Viki te čaka na Facebook-u</strong></h1>

                    <p class="mbr-text mbr-light pb-3 align-center mbr-fonts-style display-7">
                        Pridruži se mu. <a href="https://www.facebook.com/Vikikrema.si/">Klikni tu</a></p>

                </div>
            </div>
        </div>

    </section>

    <section class="mbr-gallery mbr-slider-carousel cid-rFQ5ceMPJM" id="gallery02-g">



        <div class="container">
            <div>
                <!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Awesome">
                                    <div href="#lb-gallery02-g" data-slide-to="0" data-toggle="modal"><img src="/wp-content/themes/viki/assets/images/71825559-2661128687252323-3516651967071911936-o-1200x1200-800x800.jpg" alt="" title="Drage mame, ali ste vedele, da si lahko kavo posladkate na prav poseben način? Žlička Viki kreme Premium s 30 % manj sladkorja bo vaši kavi dodala prefinjen okus lešnikov. Priporočamo. Naj bo to samo vaš čas. Kako pogosto si ga vzamete? 😉"><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Responsive">
                                    <div href="#lb-gallery02-g" data-slide-to="1" data-toggle="modal"><img src="/wp-content/themes/viki/assets/images/70185749-2610927898939069-7982879056775348224-n-960x960-800x800.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Creative">
                                    <div href="#lb-gallery02-g" data-slide-to="2" data-toggle="modal"><img src="/wp-content/themes/viki/assets/images/68731961-2585162808182245-2747951605335719936-o-1080x1080-800x800.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false" data-tags="Animated">
                                    <div href="#lb-gallery02-g" data-slide-to="3" data-toggle="modal"><img src="/wp-content/themes/viki/assets/images/57343583-2363683620330166-1295490616125292544-o-1200x1200-800x800.jpg" alt="" title=""><span class="icon-focus"></span></div>
                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery02-g">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="carousel-inner">
                                    <div class="carousel-item active"><img src="/wp-content/themes/viki/assets/images/71825559-2661128687252323-3516651967071911936-o-1200x1200.jpg" alt="" title="Drage mame, ali ste vedele, da si lahko kavo posladkate na prav poseben način? Žlička Viki kreme Premium s 30 % manj sladkorja bo vaši kavi dodala prefinjen okus lešnikov. Priporočamo. Naj bo to samo vaš čas. Kako pogosto si ga vzamete? 😉"></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/70185749-2610927898939069-7982879056775348224-n-960x960.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/68731961-2585162808182245-2747951605335719936-o-1080x1080.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/57343583-2363683620330166-1295490616125292544-o-1200x1200.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/gallery04.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/gallery05.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/gallery06.jpg" alt="" title=""></div>
                                    <div class="carousel-item"><img src="/wp-content/themes/viki/assets/images/gallery07.jpg" alt="" title=""></div>
                                </div><a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery02-g"><span class="mbri-left mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery02-g"><span class="mbri-right mbr-iconfont" aria-hidden="true"></span><span class="sr-only">Next</span></a><a class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>


<?php get_footer();