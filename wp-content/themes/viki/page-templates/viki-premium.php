<?php
/**
 * Template Name: Viki Premium
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
get_header();
wp_enqueue_style('premium-css');
wp_enqueue_style('gallery-css');
wp_enqueue_script('gallery');
?>

    <section class="header1 cid-rFTCgFwYYQ" id="header05-l" style="z-index: 10;">

        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px" height="760px"
             viewBox="0 0 1380 760" preserveAspectRatio="xMidYMid meet">
            <defs id="svgEditorDefs">
                <polygon id="svgEditorShapeDefs"
                         style="fill:khaki;stroke:black;vector-effect:non-scaling-stroke;stroke-width:0px;"></polygon>
            </defs>
            <rect id="svgEditorBackground" x="0" y="0" width="1380" height="760"
                  style="fill: none; stroke: none;"></rect>
            <path d="M0.3577131120350206,0.819491525482845h-1.5000000000000355ZM0.3577131120350206,-3.1805084745172603h-1.5000000000000355ZM-0.14228688796500222,-4.180508474517258h5.000000000000002a5,5,0,0,1,0,6.00000000000003h-5.000000000000025a5,5,0,0,0,0,-6.00000000000003ZM5.8577131120349835,-1.1805084745172634h1.0000000000000249Z"
                  style="fill:khaki; stroke:black; vector-effect:non-scaling-stroke;stroke-width:0px;" id="e2_shape"
                  transform="matrix(1.01506 82.3743 -245.478 0.34062 392.311 526.125)"></path>
        </svg>

        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-md-10 col-lg-7 align-center">
                </div>
                <div class="pt-5 align-center">
                    <img src="/wp-content/themes/viki/assets/images/viki-logo-573x302.png" alt="" title=""
                         style="width: 95%;">
                </div>

            </div>
        </div>

    </section>

    <section class="header1 cid-rFNSEmAWn9" id="header01-1" style="margin-top: -24px;">
        <svg class="svg1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px"
             height="810px" viewBox="0 0  1380 810" preserveAspectRatio="xMidYMid meet">
            <rect id="svgEditorBackground" x="0" y="0" width="1380" height="810"
                  style="fill: none; stroke: none;"></rect>
            <circle id="e1_circle" cx="309" cy="433" style="fill:aqua;stroke:black;stroke-width:0px;"
                    r="149.486"></circle>
            <circle id="e3_circle" cx="679" cy="707" style="fill: aqua; stroke-width: 0px;stroke:black;" r="516.715">
            </circle>
            <circle id="e4_circle" cx="759" cy="261" style="fill:aqua;stroke:black;stroke-width:0px;"
                    r="132.2459829258">
            </circle>
            <circle id="e5_circle" cx="1054" cy="462" style="fill:aqua;stroke:black;stroke-width:0px;"
                    r="139.413"></circle>
            <circle id="e6_circle" cx="691" cy="692" style="fill:khaki;stroke:black;stroke-width:0px;"
                    r="367.214"></circle>
        </svg>

        <div class="round round1 rev"></div>
        <div class="round round2 rev"></div>
        <div class="round round3"></div>
        <div class="round round4 rev"></div>
        <div class="round round5"></div>
        <div class="round round6 rev"></div>
        <div class="round round7"></div>
        <div class="round round8 rev"></div>
        <div class="round round9 rev"></div>
        <div class="round round10"></div>
        <div class="round round11"></div>

        <div class="container">
            <div class="row justify-content-md-center">

                <div class="col-md-10 align-center">
                    <h1 class="mbr-section-title mbr-regular pb-2 mbr-fonts-style display-1"><strong>Še boljša, še
                            lažja, z lešniki in 30% manj sladkorja.&nbsp;</strong></h1>
                    <p class="mbr-text pb-2 mbr-regular mbr-fonts-style display-5"><strong>Brez glutena &nbsp; &nbsp;
                            Brez konzervansov &nbsp; &nbsp; &nbsp; Brez umetnih sladil&nbsp;</strong></p>
                </div>
                <div class="pt-4 align-center">
                    <img src="/wp-content/themes/viki/assets/images/viki-premium03-novo-753x793.png" alt="" title=""
                         style="width: 90%;">
                </div>

            </div>
        </div>

    </section>

    <section class="cid-rFObBjs1RQ" id="header06-9">

        <div class="container align-center">
            <div class="row">
                <div class="col-md-12 col-lg-6 py-5 m-auto">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-left mbr-fonts-style display-2"><strong>Novo v
                            družini Viki</strong></h1>
                    <h3 class="mbr-section-subtitle align-left align-left mbr-regular pb-3 mbr-fonts-style display-5">
                        Nova Viki krema Premium mleko & lešniki je izvrstnega okusa. Že ko jo mažeš na kruh, zadiši
                        lešnik, zatresejo se brbončice in nestrpno pričakujejo ugriz.&nbsp;</h3>
                    <p class="mbr-text mbr-light pb-3 align-left mbr-fonts-style display-7">Ob ugrizu pa greš nazaj.
                        Nazaj v otroštvo, nazaj v igrivost, ko si brezskrbno jedel Viki kremo, tisto, ki jo vsi poznamo.
                        Premium okus doda mešanica mleka in lešnikov, ki vas bo premamila v ponovni in ponovni ugriz.
                        Kar ugriznite, brez slabe vesti. Viki Premium je lažja za kar 30 %. Prikličite si spomine in jo
                        poizkusite!</p>

                </div>

                <div class="col-lg-6 col-md-12 relative align-right">
                    <img class="img1" src="/wp-content/themes/viki/assets/images/viki_spletna_proteini.jpg" alt=""
                         title="">
                    <img class="img2" src="/wp-content/themes/viki/assets/images/viki-premium03-1-684x765.png" alt=""
                         title="">

                </div>
            </div>
        </div>
    </section>

    <section class="cid-rFQ87DgYi2" id="header08-i">

        <div class="container align-center">
            <div class="row justify-content-md-start justify-content-md-center">
                <div class="mbr-white col-sm-212 col-md-10 col-lg-7">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-center mbr-fonts-style display-5"><strong>Viki
                            Premium 30 % mleko &amp; lešniki najdete na vseh prodajnih mestih po Sloveniji.</strong>
                    </h1>
                </div>
            </div>
        </div>

    </section>

    <section class="cid-rFQ7BERBLW" id="header08-h">

        <div class="container align-center">
            <div class="row justify-content-md-start justify-content-md-center">
                <div class="mbr-white col-sm-212 col-md-10 col-lg-7">
                    <h1 class="mbr-section-title mbr-regular pb-3 align-center mbr-fonts-style display-2"><strong>Viki
                            te čaka na Facebook-u</strong></h1>

                    <p class="mbr-text mbr-light pb-3 align-center mbr-fonts-style display-7">
                        Pridruži se mu. <a href="https://www.facebook.com/Vikikrema.si/">Klikni tu</a></p>

                </div>
            </div>
        </div>

    </section>

    <section class="mbr-gallery mbr-slider-carousel cid-rFQ5ceMPJM" id="gallery02-g">

        <div class="container">
            <div>
                <!-- Gallery -->
                <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                        <div>
                            <div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false"
                                     data-tags="Awesome">
                                    <div href="#lb-gallery02-g" data-slide-to="0" data-toggle="modal"><img
                                                src="/wp-content/themes/viki/assets/images/71825559-2661128687252323-3516651967071911936-o-1200x1200-800x800.jpg"
                                                alt=""
                                                title="Drage mame, ali ste vedele, da si lahko kavo posladkate na prav poseben način? Žlička Viki kreme Premium s 30 % manj sladkorja bo vaši kavi dodala prefinjen okus lešnikov. Priporočamo. Naj bo to samo vaš čas. Kako pogosto si ga vzamete? 😉"><span
                                                class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false"
                                     data-tags="Responsive">
                                    <div href="#lb-gallery02-g" data-slide-to="1" data-toggle="modal"><img
                                                src="/wp-content/themes/viki/assets/images/70185749-2610927898939069-7982879056775348224-n-960x960-800x800.jpg"
                                                alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false"
                                     data-tags="Creative">
                                    <div href="#lb-gallery02-g" data-slide-to="2" data-toggle="modal"><img
                                                src="/wp-content/themes/viki/assets/images/68731961-2585162808182245-2747951605335719936-o-1080x1080-800x800.jpg"
                                                alt="" title=""><span class="icon-focus"></span></div>
                                </div>
                                <div class="mbr-gallery-item mbr-gallery-item--p1" data-video-url="false"
                                     data-tags="Animated">
                                    <div href="#lb-gallery02-g" data-slide-to="3" data-toggle="modal"><img
                                                src="/wp-content/themes/viki/assets/images/57343583-2363683620330166-1295490616125292544-o-1200x1200-800x800.jpg"
                                                alt="" title=""><span class="icon-focus"></span></div>
                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Lightbox -->
                <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1"
                     data-keyboard="true" data-interval="false" id="lb-gallery02-g">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="carousel-inner">
                                    <div class="carousel-item active"><img
                                                src="/wp-content/themes/viki/assets/images/71825559-2661128687252323-3516651967071911936-o-1200x1200.jpg"
                                                alt=""
                                                title="Drage mame, ali ste vedele, da si lahko kavo posladkate na prav poseben način? Žlička Viki kreme Premium s 30 % manj sladkorja bo vaši kavi dodala prefinjen okus lešnikov. Priporočamo. Naj bo to samo vaš čas. Kako pogosto si ga vzamete? 😉">
                                    </div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/70185749-2610927898939069-7982879056775348224-n-960x960.jpg"
                                                alt="" title=""></div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/68731961-2585162808182245-2747951605335719936-o-1080x1080.jpg"
                                                alt="" title=""></div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/57343583-2363683620330166-1295490616125292544-o-1200x1200.jpg"
                                                alt="" title=""></div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/gallery04.jpg" alt=""
                                                title=""></div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/gallery05.jpg" alt=""
                                                title=""></div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/gallery06.jpg" alt=""
                                                title=""></div>
                                    <div class="carousel-item"><img
                                                src="/wp-content/themes/viki/assets/images/gallery07.jpg" alt=""
                                                title=""></div>
                                </div>
                                <a class="carousel-control carousel-control-prev" role="button" data-slide="prev"
                                   href="#lb-gallery02-g"><span class="mbri-left mbr-iconfont"
                                                                aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a><a
                                        class="carousel-control carousel-control-next" role="button" data-slide="next"
                                        href="#lb-gallery02-g"><span class="mbri-right mbr-iconfont"
                                                                     aria-hidden="true"></span><span class="sr-only">Next</span></a><a
                                        class="close" href="#" role="button" data-dismiss="modal"><span class="sr-only">Close</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

<?php get_footer();