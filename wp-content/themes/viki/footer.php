<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">
	<section class="extFooter cid-rT1DEoQ4sG" id="extFooter18-2v">






		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px" height="760px" viewBox="0 0 1380 760" preserveAspectRatio="xMidYMid meet">
			<defs id="svgEditorDefs">
				<polygon id="svgEditorShapeDefs" style="fill:khaki;stroke:black;vector-effect:non-scaling-stroke;stroke-width:0px;"></polygon>
			</defs>
			<rect id="svgEditorBackground" x="0" y="0" width="1380" height="760" style="fill: none; stroke: none;"></rect>
			<path d="M0.3577131120350206,0.819491525482845h-1.5000000000000355ZM0.3577131120350206,-3.1805084745172603h-1.5000000000000355ZM-0.14228688796500222,-4.180508474517258h5.000000000000002a5,5,0,0,1,0,6.00000000000003h-5.000000000000025a5,5,0,0,0,0,-6.00000000000003ZM5.8577131120349835,-1.1805084745172634h1.0000000000000249Z" style="fill:khaki; stroke:black; vector-effect:non-scaling-stroke;stroke-width:0px;" id="e2_shape" transform="matrix(1.01506 82.3743 -245.478 0.34062 392.311 526.125)"></path>
		</svg>


		<div class="container">
			<div class="media-container-row content text-white">
				<div class="col-12 col-md-6 col-lg-4">

					<div class="media-wrap align-left">
						<a href="#">
							<img src="/wp-content/themes/viki/assets/images/viki-indijancek-698x865.png" alt="" title="">
						</a>
					</div>


					<p class="mbr-text align-left text1 mbr-fonts-style display-4">Spremljaj indijančka VIKI tudi na Facebooku.&nbsp;</p>

					<div class="social-list align-left">
						<div class="soc-item">
							<a href="https://www.facebook.com/Vikikrema.si" target="_blank">
								<span class="mbr-iconfont mbr-iconfont-social fa-facebook-square fa" style="color: rgb(87, 70, 139); fill: rgb(87, 70, 139);"></span>
							</a>
						</div>
					</div>



				</div>
				<div class="col-12 col-md-6 col-lg-4 mbr-fonts-style display-4">
					<h5 class="pb-3 align-left">
						Kontakt</h5>


					<div class="item">
						<div class="card-img"><span class="mbr-iconfont img1 mobi-mbri-map-pin mobi-mbri"></span>
						</div>
						<div class="card-box">
							<h4 class="item-title align-left mbr-fonts-style display-4">Petlja d.o.o., Ob Dravi 3a, 2250 Ptuj-SI</h4>
						</div>
					</div>

					<div class="item">
						<div class="card-img"><span class="mbr-iconfont img1 mdi-communication-email"></span></div>
						<div class="card-box">
							<h4 class="item-title align-left mbr-fonts-style display-4">info@petlja.si</h4>
						</div>
					</div>

					<div class="item">
						<div class="card-img"><span class="mbr-iconfont img1 fa-phone fa"></span>
						</div>
						<div class="card-box">
							<h4 class="item-title align-left mbr-fonts-style display-4">+386 2 788 00 30</h4>
						</div>
					</div>






				</div>

				<div class="col-12 col-md-6 col-lg-4 mbr-fonts-style display-7">
					<h5 class="pb-3 align-left">Petlja&nbsp;</h5>
					<p class="mbr-text align-left text2 mbr-fonts-style display-4">Petlja d.o.o je družinsko trgovsko -storitveno podjetje s sedežem na Ptuju, ustanovljeno 1990 leta. <strong><a href="/o-nas/">Spoznajte nas</a></strong></p>



				</div>
			</div>


		</div>
	</section>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

