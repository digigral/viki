<?php
/**
 * The template for displaying all single posts
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="single-wrapper">

    <section class="header4 cid-rR4Ht241Xu" id="content05-2l">

    <div class="container">
        <div class="row justify-content-md-center">
            <div class=" col-md-12 col-lg-10 align-left">

                <h1 class="mbr-section-title align-left mbr-white pb-2 mbr-fonts-style display-2"><?php echo $post->post_title ?></h1>

                <p class="mbr-text align-left mbr-fonts-style display-7"><?php echo get_the_date(); ?><br>
                    <?php echo $post->post_content; ?>
                    <br>
                </p>
            </div>
        </div>
    </div>

    </section>

    <section class="extFeatures cid-rR4yvWGYIo" id="extFeatures22-1w">

        <div class="container">
            <h4 class="main-title pb-5 align-left mbr-regular mbr-fonts-style display-2">Ostale novice</h4>

            <div class="row justify-content-center">
                <?php
                $args = array(
                    'post_type' => 'novice',
                    'posts_per_page' => 5
                );
                $novice = new WP_Query( $args );
                if($novice->posts):
                    foreach ($novice->posts as $post):
                        ?>
                        <div class="card p-3 col-12 col-md-6 col-lg-4">
                            <div class="card-wrapper">
                                <div class="card-img">
                                    <a href="<?php echo get_post_permalink($post ->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post ->ID)?>" alt="" title=""></a>
                                </div>
                                <div class="card-box align-left">
                                    <h4 class="card-title align-left pb-3 mbr-fonts-style display-5"><?php echo $post->post_title ?></h4>
                                    <p class="mbr-text align-left mbr-fonts-style display-4"> <?php echo $post->post_excerpt; ?></p>

                                    <div class="mbr-link-btn"><a class="btn btn-md btn-warning-outline display-4" href="<?php echo get_post_permalink($post ->ID); ?>"><span class="mobi-mbri mobi-mbri-right mbr-iconfont mbr-iconfont-btn"></span>PREBERI VEČ</a></div>

                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                endif;
                ?>
            </div>
        </div>
    </section>

</div><!-- #single-wrapper -->

<?php get_footer();
