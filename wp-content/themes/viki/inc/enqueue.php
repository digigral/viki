<?php
/**
 * UnderStrap enqueue scripts
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'understrap-styles', get_template_directory_uri() . '/css/theme.min.css', array(), $css_version );
		wp_enqueue_style( 'mobirise-styles', get_template_directory_uri() . '/css/mbr-additional.css', array(), $css_version );
		// assets
		wp_enqueue_style( 'mobirise-icons', get_template_directory_uri() . '/assets/web/assets/mobirise-icons/mobirise-icons.css', array(), $css_version );
		wp_enqueue_style( 'mobirise-icons2', get_template_directory_uri() . '/assets/web/assets/mobirise-icons2/mobirise2.css', array(), $css_version );
		wp_enqueue_style( 'tether', get_template_directory_uri() . '/assets/tether/tether.min.css', array(), $css_version );
		wp_enqueue_style( 'dropdown', get_template_directory_uri() . '/assets/dropdown/css/style.css', array(), $css_version );
		wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/assets/animatecss/animate.min.css', array(), $css_version );
		wp_enqueue_style( 'my-css', get_template_directory_uri() . '/assets/theme/css/style.css', array(), $css_version );


        //protein
        wp_register_style( 'protein-css', get_template_directory_uri() . '/assets/protein/assets/mobirise/css/mbr-additional.css', array(), $css_version );
        //premium
        wp_register_style( 'premium-css', get_template_directory_uri() . '/assets/premium/assets/mobirise/css/mbr-additional.css', array(), $css_version );

        wp_register_style( 'gallery-css', get_template_directory_uri() . '/assets/premium/assets/gallery/style.css', array(), $css_version );

        $js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );
        wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
		wp_enqueue_script( 'tether', get_template_directory_uri() . '/assets/tether/tether.min.js', array(), $js_version, true );
		wp_enqueue_script( 'popper', get_template_directory_uri() . '/assets/popper/popper.min.js', array(), $js_version, true );
		wp_enqueue_script( 'dropdown', get_template_directory_uri() . '/assets/dropdown/js/nav-dropdown.js', array(), $js_version, true );
        wp_enqueue_script( 'navbar', get_template_directory_uri() . '/assets/dropdown/js/navbar-dropdown.js', array(), $js_version, true );
		wp_enqueue_script( 'touchswipe', get_template_directory_uri() . '/assets/touchswipe/jquery.touch-swipe.min.js', array(), $js_version, true );
		wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/assets/smoothscroll/smooth-scroll.js', array(), $js_version, true );
		wp_enqueue_script( 'viewportchecker', get_template_directory_uri() . '/assets/viewportchecker/jquery.viewportchecker.js', array(), $js_version, true );
		wp_enqueue_script( 'parallax', get_template_directory_uri() . '/assets/parallax/jarallax.min.js', array(), $js_version, true );
		wp_enqueue_script( 'playervimeo', get_template_directory_uri() . '/assets/playervimeo/vimeo_player.js', array(), $js_version, true );
		wp_enqueue_script( 'myscript', get_template_directory_uri() . '/assets/theme/js/script.js', array(), $js_version, true );

		wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/src/js/custom-script.js', array(), $js_version, true );

        wp_register_script( 'gallery', get_template_directory_uri() . '/assets/premium/assets/gallery/script.js', array(), $js_version, true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
