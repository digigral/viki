<?php

function register_my_posttypes()
{

    /*
     *
     * example cpt
     * replace example with your own cpt labels
       * replace digi with your own text domain
     */

    $labels = array(
        'name'               => _x( 'Novice', 'post type general name', 'digi' ),
        'singular_name'      => _x( 'Novice', 'post type singular name', 'digi' ),
        'menu_name'          => _x( 'Novice', 'admin menu', 'digi' ),
        'name_admin_bar'     => _x( 'Novice', 'add new on admin bar', 'mdigi' ),
        'add_new'            => _x( 'Add new', 'novica', 'digi' ),
        'add_new_item'       => __( 'Add new novica', 'digi' ),
        'new_item'           => __( 'New Novice', 'digi' ),
        'edit_item'          => __( 'Edit Novice', 'digi' ),
        'view_item'          => __( 'View Novice', 'digi' ),
        'all_items'          => __( 'All Novice', 'digi' ),
        'search_items'       => __( 'Search Novice', 'digi' ),
        'parent_item_colon'  => __( 'Parent Novice:', 'digi' ),
        'not_found'          => __( 'Not found', 'digi' ),
        'not_found_in_trash' => __( 'Not found in trash', 'digi' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'novice' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'taxonomies' => array('post_tag'),
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
    );

    register_post_type( 'novice', $args );


//    register_taxonomy(
//        'kategorija',
//        'Novice',
//        array(
//            'rewrite' => array(
//                "slug" => 'kategorija'
//            ),
//            'label' => __("Kategorija"),
//            'hierarchical' => true
//        )
//    );
//    register_taxonomy_for_object_type( 'kategorija_Novice', 'Novice' );

    $labels = array(
        'name'               => _x( 'Recepti', 'post type general name', 'digi' ),
        'singular_name'      => _x( 'Recepti', 'post type singular name', 'digi' ),
        'menu_name'          => _x( 'Recepti', 'admin menu', 'digi' ),
        'name_admin_bar'     => _x( 'Recepti', 'add new on admin bar', 'mdigi' ),
        'add_new'            => _x( 'Add new', 'recept', 'digi' ),
        'add_new_item'       => __( 'Add new recept', 'digi' ),
        'new_item'           => __( 'New Recepti', 'digi' ),
        'edit_item'          => __( 'Edit Recepti', 'digi' ),
        'view_item'          => __( 'View Recepti', 'digi' ),
        'all_items'          => __( 'All Recepti', 'digi' ),
        'search_items'       => __( 'Search Recepti', 'digi' ),
        'parent_item_colon'  => __( 'Parent Recepti:', 'digi' ),
        'not_found'          => __( 'Not found', 'digi' ),
        'not_found_in_trash' => __( 'Not found in trash', 'digi' )
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'recepti' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'taxonomies' => array('post_tag'),
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions' )
    );

    register_post_type( 'recepti', $args );

}

add_action( 'init', 'register_my_posttypes' );