<?php
/**
 * The template for displaying archive pages
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="wrapper" id="archive-wrapper">
    <div class="container">
        <div class="row justify-content-center">
        <?php if ( have_posts() ) : ?>
            <?php /* Start the Loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <div class="card p-3 col-12 col-md-6 col-lg-4">
                    <div class="card-wrapper">
                        <div class="card-img">
                            <a href="<?php echo get_post_permalink($post ->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post ->ID)?>" alt="" title=""></a>
                        </div>
                        <div class="card-box align-left">
                            <h4 class="card-title align-left pb-3 mbr-fonts-style display-5"><?php echo $post->post_title ?></h4>
                            <p class="mbr-text align-left mbr-fonts-style display-4"><?php echo $post->post_excerpt; ?></p>
                            <div class="mbr-link-btn"><a class="btn btn-md btn-warning-outline display-4" href="<?php echo get_post_permalink($post ->ID); ?>"><span class="mobi-mbri mobi-mbri-right mbr-iconfont mbr-iconfont-btn"></span>PREBERI VEČ</a></div>

                        </div>
                    </div>
                </div>
            <?php endwhile;
            endif;?>
            </div>

        </div>
    </div>
</div><!-- #archive-wrapper -->

<?php get_footer();
