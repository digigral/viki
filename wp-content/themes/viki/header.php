<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="/wp-content/themes/viki/assets/images/favicon-16x16.png" type="image/x-icon">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

	<!-- ******************* The Navbar Area ******************* -->
	<section class="menu cid-rR4sPch9jO" once="menu" id="menu01-1u">
		<nav class="navbar navbar-dropdown navbar-fixed-top navbar-expand-lg">
			<div class="navbar-brand">
            <span class="navbar-logo">
                <a href="/">
                    <img src="/wp-content/themes/viki/assets/images/viki-logo-2-122x51.png" alt="" title="" style="height: 3.8rem;">
                </a>
            </span>

			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
				<div class="hamburger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                <?php
                 $menu = get_field('menu_r','options');
                if($menu):
                    foreach ($menu as $i):
                        $subitems = $i['podstran_r'];
                ?>
                    <li class="nav-item <?php if($subitems){echo 'dropdown';}?>">
                        <a class="nav-link link text-white display-7  <?php if($subitems){echo 'dropdown-toggle';}?>" href="<?php echo $i['link_strani'];?>" aria-expanded="false" <?php if($subitems){echo 'data-toggle="dropdown-submenu"';}?>>
							<?php echo $i['ime_strani'];?>
                        </a>
                        <?php if($subitems):?>
                         <div class="dropdown-menu">
                             <?php
                            foreach ($subitems as $subi):
						    ?>
							<a class="dropdown-item text-white display-7" href="<?php echo $subi['link_strani'];?>" aria-expanded="false"><?php echo $subi['ime_strani'];?></a>
                             <?php endforeach; ?>
						</div>
                        <?php endif; ?>
					</li>
                <?php endforeach;
                endif;
                ?>
				<div class="navbar-buttons px-2 mbr-section-btn"><a class="btn btn-sm btn-primary display-4" href="mailto:melita@petlja.si">
						PIŠITE NAM</a></div>
			</div>
		</nav>
	</section>

 <?php
 if(get_page_template_slug() != "page-templates/viki-protein.php" && get_page_template_slug() != "page-templates/viki-premium.php"): ?>
	<section class="header1 cid-rR1Rl8hHXe mbr-parallax-background" id="header05-13">
		<div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(113, 108, 128);">
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1380px" height="760px" viewBox="0 0 1380 760" preserveAspectRatio="xMidYMid meet">
			<defs id="svgEditorDefs">
				<polygon id="svgEditorShapeDefs" style="fill:khaki;stroke:black;vector-effect:non-scaling-stroke;stroke-width:0px;"></polygon>
			</defs>
			<rect id="svgEditorBackground" x="0" y="0" width="1380" height="760" style="fill: none; stroke: none;"></rect>
			<path d="M0.3577131120350206,0.819491525482845h-1.5000000000000355ZM0.3577131120350206,-3.1805084745172603h-1.5000000000000355ZM-0.14228688796500222,-4.180508474517258h5.000000000000002a5,5,0,0,1,0,6.00000000000003h-5.000000000000025a5,5,0,0,0,0,-6.00000000000003ZM5.8577131120349835,-1.1805084745172634h1.0000000000000249Z" style="fill:khaki; stroke:black; vector-effect:non-scaling-stroke;stroke-width:0px;" id="e2_shape" transform="matrix(1.01506 82.3743 -245.478 0.34062 392.311 526.125)"></path>
		</svg>

		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-md-10 col-lg-7 align-center">
				</div>
				<div class="pt-5 align-center">
                    <a href="/"> <img src="/wp-content/themes/viki/assets/images/viki-logo-2-573x238.png" alt="" title="" style="width: 95%;"></a>
				</div>
			</div>
		</div>
	</section>
    <?php
    endif;


